import React from "react";
import { Link, useHistory } from "react-router-dom";
import "./nav.css";
function Navigation() {
  let history = useHistory();
  return (
    <>
      <div className="container-fluid" id="sticky">
        <nav className="navbar navbar-expand-lg navbar-light">
          <img
            src="https://crystaldelta.com/wp-content/themes/CrystalDelta/img/logo/crystal-delta_b.png"
            id="loogo"
            className=" loogo p-1"
            style={{ width: "50px", height: "50px", borderRadius: "1px solid" }}
          />
          <div class="container-md">
            <a className="navbar-brand brand" href="/">
              <h4 className="mt-2 logo">E- LEARNING</h4>
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbar"
              aria-controls="navbar"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbar">
              <ul className="navbar-nav ms-auto">
                <li className="nav-item ms-4">
                  <Link
                    to="/AllCourse"
                    className="nav-link text-center nav-links text-white"
                    style={{ fontWeight: 460, fontSize: 20 }}
                  >
                    All Course
                  </Link>
                </li>

                <li className="nav-item ms-4">
                  <Link
                    to="/Dashboard"
                    className="nav-link text-center nav-links text-center text-white"
                    style={{ fontWeight: 460, fontSize: 20 }}
                  >
                    My Dashboard
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </>
  );
}

export default Navigation;
