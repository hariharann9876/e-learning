import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";

function Example(props) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  console.log(props.data);

  return (
    <>
      <center>
        <Button
          variant="primary"
          className="mt-3"
          onClick={handleShow}
          style={{ width: "200px" }}
        >
          View
        </Button>
      </center>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        scrollable={true}
      >
        <Modal.Header closeButton>
          <Modal.Title>{props.data.name}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <center>
            <iframe
              src={props.data.video}
              title="COURSE"
              style={{ height: "300px" }}
            ></iframe>
          </center>
          <p className="mt-5">
            {" "}
            <b>DESCRIPTION</b> : <br /> {props.data.content}
          </p>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary">Un-Enroll Now</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Example;
