import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import DataJson from "./array.json";

function Example(props) {
  const [show, setShow] = useState(false);
  const [course, setCourse] = useState(DataJson);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleEnroll = () => {
    var obj = {
      content: props.data.description,
      image: props.data.photo,
      video: props.data.video,
      name: props.data.name,
    };
    course.post.push(obj);
    setShow(false);
    alert("Enroll Successfully");
  };
  return (
    <>
      <center>
        <Button
          variant="primary"
          onClick={handleShow}
          style={{ width: "200px" }}
        >
          Enroll-now
        </Button>
      </center>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        scrollable={true}
      >
        <Modal.Header closeButton>
          <Modal.Title>{props.data.name}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <center>
            <img
              src={props.data.photo}
              className="img-fluid"
              style={{ height: "200px", objectFit: "contain" }}
            />
          </center>
          <h4 className="mt-5">Hours : {props.data.hours}</h4>
          <p className="mt-3">
            {" "}
            <b>Description</b> : <br /> {props.data.description}
          </p>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleEnroll}>
            Enroll Now
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Example;
