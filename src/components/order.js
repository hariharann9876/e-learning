import React, { useState } from "react";
import Modal2 from "../components/modal2";
import DataJson from "./array.json";
export default function Orders() {
  const [data, setData] = useState(DataJson);
  const render = data?.post?.map((post) => (
    <>
      <div className="col-lg-3 col-xs-12 col-md-6">
        <div class="card mt-3">
          <img
            src={post.image}
            class="card-img "
            style={{ height: "150px", objectFit: "cover" }}
          />
          <div class="card-body">
            <h5 class="card-title">{post.name}</h5>
            <Modal2 data={post} />
          </div>
        </div>
      </div>
    </>
  ));

  return (
    <div style={{ overflow: "hidden" }}>
      <br />
      <br />
      <br />
      <h4 className="display-6 text-center mt-3" style={{ fontWeight: "bold" }}>
        MY-COURSES
      </h4>
      <div class="row p-5">{render}</div>
    </div>
  );
}
