import React from "react";
import { useHistory } from "react-router-dom";
import "./components/nav.css";

const Signin = (props) => {
  let history = useHistory();
  return (
    <>
      <div className="container-sm mt-4 p-3">
        <div class="rounded p-3">
          <div class="center d-flex row ">
            <div className="col-3"></div>
            <div class="col-lg-6 col-sm-8 col-sm-10 mt-3">
              <div class="shadow-lg p-4">
                <h1 class="mt-3">Sign-up</h1>
                <form class="form">
                  <input
                    type="text"
                    name="name"
                    class="w-100 input mt-3"
                    placeholder="Username"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                  />
                  <input
                    type="text"
                    name="email"
                    class="w-100 input mt-3"
                    placeholder=" Email"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                  />

                  <input
                    type="password"
                    name="password"
                    class="w-100 input mt-3"
                    placeholder="Password"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                  />

                  <input
                    type="text"
                    name="phone"
                    class="w-100 input mt-3"
                    placeholder="phone"
                    style={{
                      padding: "13px",
                      borderRadius: "5px",
                      border: "1px solid grey",
                    }}
                  />
                  <br />
                  <button
                    type="submit"
                    class="w-100 text-white  btn  mt-3"
                    style={{ padding: "13px", backgroundColor: "#382f9c" }}
                  >
                    <b> Create Account</b>
                  </button>
                  <br />
                  <button
                    class="w-100 text-white  btn btn-primary mt-3"
                    style={{ padding: "13px" }}
                    onClick={() => {
                      history.push("/register");
                    }}
                  >
                    Login
                  </button>
                  <br />
                </form>
              </div>
            </div>
            <div className="col-3"></div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Signin;
