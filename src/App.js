import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navigation from "./components/content";
import Order from "./components/order";
import Service from "./pages/service";
export default function App() {
  return (
    <Router>
      <Navigation />
      <Switch>
        <Route path="/" exact component={Service} />
        <Route path="/AllCourse" component={Service} />
        <Route path="/Dashboard" component={Order} />
      </Switch>
    </Router>
  );
}