import React from "react";
import { useHistory } from "react-router-dom";
import "./components/nav.css";

function Signin() {
  let history = useHistory();

  return (
    <>
      <div className="container-sm mt-4 p-3">
        <div class="center rounded d-flex row ">
          <div className="col-md-3 col-sm-0"></div>
          <div class="col-lg-6 col-sm-8 col-sm-10 mt-3">
            <div class="shadow-lg p-4">
              <h1 class="mt-3">Sign-in</h1>
              <form class="form">
                <input
                  type="text"
                  name="name"
                  class="w-100 input mt-3"
                  placeholder="Username or Email"
                  style={{
                    padding: "13px",
                    borderRadius: "5px",
                    border: "1px solid grey",
                  }}
                />

                <input
                  type="password"
                  name="password"
                  class="w-100 input mt-3"
                  placeholder="Password"
                  style={{
                    padding: "13px",
                    borderRadius: "5px",
                    border: "1px solid grey",
                  }}
                />

                <button
                  type="submit"
                  class="w-100 text-white  btn  mt-3"
                  style={{ padding: "13px", backgroundColor: "#382f9c" }}
                  onClick={() => {
                    history.push("/AllCourse");
                  }}
                >
                  <b>Signin</b>
                </button>
                <br />
                <button
                  class="w-100 text-white  btn btn-primary mt-3"
                  style={{ padding: "13px" }}
                  onClick={() => {
                    history.push("/login");
                  }}
                >
                  Create an account
                </button>

                <br />
              </form>
            </div>
          </div>
          <div className="col-md-3 col-sm-0"></div>
        </div>
      </div>
    </>
  );
}

export default Signin;
