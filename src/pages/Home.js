import React from "react";
import { BsCalendar } from "react-icons/bs";
import { GiCash } from "react-icons/gi";
import { GrServices } from "react-icons/gr";
import Footer from "../components/footer";
import "./pages.css";
export default function App() {
  return (
    <div style={{ overflow: "hidden" }}>
      <div className="backimg col-lg-12 col-md-12 col-sm-12  ">
        <div id="homedesign">
          <div className="container-fluid">
            <div className="row mt-5">
              <div className="col-12 " style={{ textAlign: "center" }}>
                <center>
                  <span className="h5 title1 text-dark">
                    . We Can Fix Anything .
                  </span>
                  <p className="title display-1 mt-3">CUSTOM YOUR</p>
                  <p className="para display-5">-------motor cycle-------</p>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row p-4 smimg ">
        {/* 1st */}
        <div className="col-lg-4 col-md-4 col-sm-12 ">
          <img
            src="https://media.istockphoto.com/photos/mechanic-repairing-a-motorcycle-picture-id1188820923?b=1&k=20&m=1188820923&s=170667a&w=0&h=OTL2yvcNEp2SMhMayIsZeePDaLqI-01fWc-YzmPicaI="
            class="img-thumbnail"
            alt="..."
          />
          <div style={{ marginTop: "20px" }}>
            <b>GENERAL SERVICES</b>
          </div>
        </div>
        {/* 2nd */}
        <div className="col-lg-4 col-md-4 col-sm-12 ">
          <img
            src="https://media.istockphoto.com/photos/motorcycle-car-wash-motorcycle-big-bike-cleaning-with-foam-injection-picture-id1255613189?b=1&k=20&m=1255613189&s=170667a&w=0&h=fCesSplCKoaQ7tC_87KuYJ2tTWw9oDMSoGvw47JmnpM="
            class="img-thumbnail"
            alt="..."
          />
          <div style={{ marginTop: "20px" }}>
            <b>WATER WASH</b>
          </div>
        </div>
        {/* 3rd */}
        <div className="col-lg-4 col-md-4 col-sm-12  ">
          <img
            src="https://media.istockphoto.com/photos/the-process-of-pouring-new-oil-into-the-motorcycle-engine-picture-id1174788025?b=1&k=20&m=1174788025&s=170667a&w=0&h=iVCD65xtcbrFX3fiwIL8MRwJeRVS6HF-dbGFu3RbN2E="
            class="img-thumbnail"
            alt="..."
          />
          <div style={{ marginTop: "20px" }}>
            <b>OIL SERVICE</b>
          </div>
        </div>
      </div>
      {/*  */}
      <div className="row p-5" style={{ backgroundColor: "#EBEDF5" }}>
        <center>
          <h5 className="text-center display-4" style={{ fontWeight: "bold" }}>
            OUR SPECALITY
          </h5>
        </center>
        <div className="col-lg-4 p-5 col-md-4 col-sm-12">
          <div class="card" style={{ borderRadius: "15px" }}>
            <h1 className="text-center mt-5">
              <BsCalendar style={{ fontSize: "60px" }} />
            </h1>
            <div class="card-body">
              <h5 class="card-title text-center">Quick Service</h5>
            </div>
          </div>
        </div>
        {/*  */}
        <div className="col-lg-4 p-5 col-md-4 col-sm-12">
          <div class="card" style={{ borderRadius: "15px" }}>
            <h1 className="text-center mt-5">
              <GrServices style={{ fontSize: "60px" }} />
            </h1>
            <div class="card-body">
              <h5 class="card-title text-center">Quality Service</h5>
            </div>
          </div>
        </div>
        {/*  */}

        <div className="col-lg-4 p-5 col-md-4 col-sm-12">
          <div class="card" style={{ borderRadius: "15px" }}>
            <h1 className="text-center mt-5">
              <GiCash style={{ fontSize: "60px" }} />
            </h1>
            <div class="card-body">
              <h5 class="card-title text-center">Money Saving</h5>
            </div>
          </div>
        </div>
      </div>
      <div className="row p-5">
        <h5 className="text-center display-4" style={{ fontWeight: "bold" }}>
          Our Brands
        </h5>
        <div className="col-12 mt-5">
          <img
            style={{ objectFit: "cover" }}
            src="http://motorcycle-brands.com/wp-content/uploads/2016/08/motorcycle-brands.png"
            height="100%"
            width="100%"
          />
        </div>
      </div>
      <Footer />
    </div>
  );
}
