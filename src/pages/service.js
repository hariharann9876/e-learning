import React from "react";
import Data from "../components/data.json";
import Footer from "../components/footer";
import Modals from "../components/modal";
import "./pages.css";
export default function Service() {
  return (
    <>
      <div
        className="container-fluid"
        style={{ overflow: "hidden", backgroundColor: "#EBEDF5" }}
      >
        <h3
          className="display-5 text-center text-warning"
          style={{ fontWeight: "bold", color: "yellow" }}
        >
          Near By You
        </h3>
        <div className="row p-5">
          {Data?.length ? (
            Data.map((datas) => (
              <div className="col-lg-3 col-xs-12 col-md-6">
                <div class="card mt-3">
                  <img
                    src={datas.photo}
                    class="card-img "
                    style={{ height: "150px", objectFit: "cover" }}
                  />
                  <div class="card-body">
                    <h5 class="card-title">{datas.name}</h5>
                    <h6>prize : &#8377; {datas.prize} </h6>
                    <div class="mb-5"></div> <Modals data={datas} />
                  </div>
                </div>
              </div>
            ))
          ) : (
            <>
              <center>" "</center>
            </>
          )}
        </div>

        <Footer />
      </div>
    </>
  );
}
