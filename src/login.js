import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Apps from "./App";
import Signin from "./signin";
import Signup from "./signup";

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/AllCourse" component={Apps} />
        <Route path="/" exact component={Signin} />
        <Route path="/login" component={Signup} />
        <Route path="/register" component={Signin} />
      </Switch>
    </Router>
  );
}
